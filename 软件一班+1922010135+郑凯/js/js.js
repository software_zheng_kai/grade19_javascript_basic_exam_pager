// 1、题目描述
// 找出元素 item 在给定数组 arr 中的位置
// 输出描述:
// 如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
// 示例1
// 输入
// [ 1, 2, 3, 4 ], 3
// 输出
// 2
//答案：
// let arr=[1,2,3,4];
// let item=3;
// console.log(arr.indexOf(item));

// 2、题目描述
// 计算给定数组 arr 中所有元素的总和
// 输入描述:
// 数组中的元素均为 Number 类型
// 示例1
// 输入
// [ 1, 2, 3, 4 ]
// 输出
// 10
//答案：
// let arr=[1,2,3,4];
// let num=0;
// for(let i=0;i<arr.length;i++){
//     num=num+arr[i];
// }
// console.log(num);

// 3、题目描述
// 移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
// 示例1
// 输入
// [1, 2, 3, 4, 2], 2
// 输出
// [1, 3, 4]
//答案：
// let arr=[1, 2, 2, 3, 4, 2, 2];
// let arr1=[];
// for(i=0;i<arr.length;i++){
//     if(arr[i]!=2){
//         arr1[arr1.length]=arr[i];
//     }
// }
// console.log(arr1);


// 4、题目描述
// 移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
// 示例1
// 输入
// [1, 2, 2, 3, 4, 2, 2], 2
// 输出
// [1, 3, 4]
//答案:
// let arr=[1, 2, 2, 3, 4, 2, 2];
// let arr1=[];
// for(i=0;i<arr.length;i++){
//     if(arr[i]!=2){
//         arr1[arr1.length]=arr[i];
//     }
// }
// console.log(arr1);

// 5、题目描述
// 在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
// 示例1
// 输入
// [1, 2, 3, 4],  10
// 输出
// [1, 2, 3, 4, 10]
//答案：
//  let arr=[1, 2, 3, 4];
//  let item=10;
//  let arr1=arr.push(item);
//  console.log(arr);

// 6、题目描述
// // 删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
// // 示例1
// // 输入
// // [1, 2, 3, 4]
// // 输出
// // [1, 2, 3]
//答案：
// let arr=[1, 2, 3, 4];
// let arr1=arr.pop();
// console.log(arr);

// 7、题目描述
// 在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
// 示例1
// 输入
// [1, 2, 3, 4], 10
// 输出
// [10, 1, 2, 3, 4]
//答案:
// let arr=[1, 2, 3, 4];
// let item=10;
// let arr1=arr.unshift(item);
// console.log(arr);

// 8、题目描述
// 删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
// 示例1
// 输入
// [1, 2, 3, 4]
// 输出
// [2, 3, 4]
//答案：
// let arr=[1, 2, 3, 4];
// let item=arr.shift();
// console.log(arr);

// 9、题目描述
// 合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
// 示例1
// 输入
// [1, 2, 3, 4], ['a', 'b', 'c', 1]
// 输出
// [1, 2, 3, 4, 'a', 'b', 'c', 1]
//答案：
// let arr1=[1, 2, 3, 4];
// let arr2=['a', 'b', 'c', 1];
// let arr=arr1.concat(arr2);
// console.log(arr);

// 10、题目描述
// 在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
// 示例1
// 输入
// [1, 2, 3, 4], 'z', 2
// 输出
// [1, 2, 'z', 3, 4]
//答案：
// let arr=[1, 2, 3, 4];
// let arr1=arr.splice(2,0,'z');
// console.log(arr);

// 11、题目描述
// 统计数组 arr 中值等于 item 的元素出现的次数
// 示例1
// 输入
// [1, 2, 4, 4, 3, 4, 3], 4
// 输出
// 3
//答案：
// let arr=[1, 2, 4, 4, 3, 4, 3];
// let item=4;
// let count=0;
// for(let i=0;i<arr.length;i++){
//     if(arr[i]==item){
//         count++;
//     }
// }
// console.log(count);

// 12、题目描述
// 找出数组 arr 中重复出现过的元素
// 示例1
// 输入
// [1, 2, 4, 4, 3, 3, 1, 5, 3]
// 输出
// [1, 3, 4]
// let arr=[1, 2, 4, 4, 3, 3, 1, 5, 3];
// let item=arr.sort();
// let a=[];
// for(let i=0;i<arr.length;i++){
//     if(item>1){
        
//     }
// }
// console.log(arr);

// 13、题目描述
// 为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
// 示例1
// 输入
// [1, 2, 3, 4]
// 输出
// [1, 4, 9, 16]
// let arr=[1, 2, 3, 4];
// let arr1=arr.slice();
// for(let i=0;i<arr1.length;i++){
//     arr[i]=arr[i]*arr[i];
// }
// console.log(arr);
// 14、题目描述
// 在数组 arr 中，查找值与 item 相等的元素出现的所有位置
// 示例1
// 输入
// ['a','b','c','d','e','f','a','b','c'] 'a'
// 输出
// [0, 6]

// 15、题目描述
// 判断 val1 和 val2 是否完全等同
// let val1=[1,264,64,6];
// let val2=[5,6,56,5];
// if(val1==val2){
//     console.log('完全等同');
// }else{
//     console.log('不等同');
// }

// 16、题目描述
// 定义一个计算圆面积的函数area_of_circle()，它有两个参数：
// r: 表示圆的半径；
// pi: 表示π的值，如果不传，则默认3.14
//答案：
// let r=parseFloat(prompt('输入圆的半径'));
// let pi=3.14;
// function area_of_circle(r){    
//     return r*r*pi;
// }
// console.log(area_of_circle(r));

// 17、题目描述
// <!-- HTML结构 -->
// <ul id="test-list">
//     <li>JavaScript</li>
//     <li>Swift</li>
//     <li>HTML</li>
//     <li>ANSI C</li>
//     <li>CSS</li>
//     <li>DirectX</li>
// </ul>
// 针对以上文档结构，请把与Web开发技术不相关的节点删掉：
// 注意！！请分别使用原生JS和jQuery两种试实现！！！！！
//答案：

// 18、题目描述

// 对如下的Form表单：

// <!-- HTML结构 -->
// <form id="test-form" action="test">
//     <legend>请选择想要学习的编程语言：</legend>
//     <fieldset>
//         <p><label class="selectAll"><input type="checkbox"> <span class="selectAll">全选</span><span class="deselectAll">全不选</span></label> <a href="#0" class="invertSelect">反选</a></p>
//         <p><label><input type="checkbox" name="lang" value="javascript"> JavaScript</label></p>
//         <p><label><input type="checkbox" name="lang" value="python"> Python</label></p>
//         <p><label><input type="checkbox" name="lang" value="ruby"> Ruby</label></p>
//         <p><label><input type="checkbox" name="lang" value="haskell"> Haskell</label></p>
//         <p><label><input type="checkbox" name="lang" value="scheme"> Scheme</label></p>
// 		<p><button type="submit">Submit</button></p>
//     </fieldset>
// </form>

// 要求:
// 绑定合适的事件处理函数，实现以下逻辑：

// 当用户勾上“全选”时，自动选中所有语言，并把“全选”变成“全不选”；

// 当用户去掉“全不选”时，自动不选中所有语言；

// 当用户点击“反选”时，自动把所有语言状态反转（选中的变为未选，未选的变为选中）；

// 当用户把所有语言都手动勾上时，“全选”被自动勾上，并变为“全不选”；

// 当用户手动去掉选中至少一种语言时，“全不选”自动被去掉选中，并变为“全选”。